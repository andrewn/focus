"use strict";

var passiveEventListener = require('./utils/passive-event-listener');
var rafUtils = require('./utils/raf-utils');

function EyeballsFocus(callback) {

  // Add listeners to the base window
  passiveEventListener.addEventListener(window, 'focus', function() {
    callback(true);
  });

  passiveEventListener.addEventListener(window, 'blur', function() {
    callback(false);
  });

}

module.exports = EyeballsFocus;
